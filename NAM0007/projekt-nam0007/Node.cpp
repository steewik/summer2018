#include "stdafx.h"
#include "Node.h"


Node::Node(int value)
{
	this->value = value;
	this->left = nullptr;
	this->right = nullptr;
}

Node::~Node()
{
	if (this->left != nullptr)
		delete this->left;

	if (this->right != nullptr)
		delete this->right;
}



bool Node::IsLeaf()
{
	if (this->left == nullptr && this->right == nullptr)
		return true;

	return false;
}

bool Node::HasLeft()
{
	if (this->left == nullptr)
		return false;

	return true;
}

bool Node::HasRight()
{
	if (this->right == nullptr)
		return false;

	return true;
}



bool Node::CompareStructure(Node *a, Node *b)
{
	if (a == nullptr && b == nullptr)
		return true;

	if (a == nullptr || b == nullptr)
		return false;

	if (a->HasLeft() != b->HasLeft())
		return false;

	if (a->HasRight() != b->HasRight())
		return false;

	return true;
}