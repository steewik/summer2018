#include <iostream>
#include <stdlib.h>
#include <time.h>
// T��da ve t��d�
class BSTree
{
private:
	class Node {
		public:
			int key; // Vlozene cislo
			Node* left = nullptr; // Uzel koukne doleva
			Node* right = nullptr; // Uzel koukne doprava
			Node(int key);
			void InOrder(); // Razeni
			void deleteTree(Node*& root); // Maz�n�
	};

	void Next(BSTree::Node* prev); // Proch�zen�
	void insert(int key, Node*& root); // vkl�dan�
	Node** stak = nullptr; //Zasobnik
	int capacity = 50; // Velikost
	int top = 0; // Nejvy��� prvek
	void push(Node* value);//Pr�chod dovnit�
	Node* pop(); // Pr�chod zp�t
	Node* root = nullptr; // hlavni uzel
	void insert(int key); // Vkl�dan� samostatn�ch prvku

public:
	BSTree();
	void fillTree();
	Node* GetRoot();
	void io();
	void Reset();
	int CurrentKey();
	bool IsEnd();
	void Next();
	~BSTree();
};

using namespace std;

int main() {
	BSTree* tree = new BSTree;

	tree->fillTree();

	cout << endl << "Inorder zapis: " << endl;
	tree->io();

	cout <<endl<< endl << "Hlavni uzel: " << tree->GetRoot()->key << endl;

	cout << "Reset: " << endl;;
	tree->Reset();
	cout << endl;
   // Vypis proch�zen�
	for(int i=0;i<15;i++)
    {

	cout << endl << "Momentalni uzel: " << tree->CurrentKey() <<endl;
	//tree->IsEnd();
	tree->Next();

    }
	delete tree;

	system("pause");
	return 0;
}



BSTree::Node::Node(int key) {
	this->key = key;
}

void BSTree::insert(int key) {
	this->insert(key, this->root);
}

void BSTree::insert(int key, Node*& root) {
	if (root == nullptr) {
		root = new Node(key);
		cout << "Vlozene cislo: " << root->key << endl;
		return;
	}

	if(root->key == key) {
		return;
	}

	if (root->key < key) {
		insert(key, root->right);
	}

	if (root->key > key) {
		insert(key, root->left);
	}

}

int BSTree::CurrentKey() {
	if(this->top-1 >= 0)
		return (this->stak[top-1]->key);
	return -666;
}

bool BSTree::IsEnd() {
	if(top)
		return false;
	return true;
}

void BSTree::Next(BSTree::Node* prev) {
	if (!top) {
		return;
	}
	Node* curr = this->stak[top - 1];
	bool doPop = false;
	if (prev != nullptr) {

		if (curr->right != nullptr) {

			if (prev->key == curr->right->key) {
				doPop = true;
			}
			else return;
		}
		if (curr->left != nullptr) {

			if (prev->key == curr->left->key) {
				return;
			}
		}
	}
	if (curr->right == nullptr || doPop) {
		pop();
		this->Next(curr);
	}
	else {
		Node* right = curr->right;
		BSTree::push(right);
		while (right->left != nullptr) {
			push(right->left);
			right = right->left;

		}
	}
}

void BSTree::Next() {
	Next(nullptr);
}

// Napln�n� random �isla
void BSTree::fillTree() {
	int x;
	bool duplicity = false;
	int maxNode = 10;
	int *nodeX = new int[maxNode + 1];
	nodeX[0] = 5;

	srand(time(NULL));
	for (int i = 1; i <= maxNode; i++) {
		x = rand() % 20 + 0;
		duplicity = false;
		for (int j = 0; j < i; j++) {
			if (x == nodeX[j]) {
				duplicity = true;
				break;
			}
		}
		if (duplicity) {
			i--;
			continue;
		}
		else {
			nodeX[i] = x;
			this->insert(x);
		}
	}
	delete[] nodeX;

}

void BSTree::push(Node* value) {
	if (this->top <= this->capacity) {
		this->stak[this->top++] = value;
		cout << "Push: " << this->stak[this->top-1]->key << endl;
	}
	else {
		cout << "Zasobnik je plny" << endl;
	}
}

BSTree::Node* BSTree::pop()
{
	if (this->top > 0)
	{
		this->top--;
		cout << "Pop: " << stak[this->top]->key <<endl;
		Node* toRet = stak[this->top];
		stak[this->top] = nullptr;
		return(toRet);
	}
	else
	{
		cout << "Zasobnik je pr�zdn� " << endl;
		return (nullptr);
	}

}

void BSTree::Reset() {
		for(int i = 0; i < top; i++) {
				pop();
		}

	Node* curr = this->root;

	while(curr != nullptr) {
		push(curr);
		curr = curr->left;
	}
	delete[] curr;
	curr = nullptr;
}
//�azen�
void BSTree::Node::InOrder() {
	if (this->left != nullptr)
		this->left->InOrder();
	cout << this->key << ", ";

	if (this->right != nullptr)
		this->right->InOrder();
}

void BSTree::io() {
	this->root->InOrder();
}

BSTree::Node* BSTree::GetRoot() {
	return this->root;
}

BSTree::BSTree()
{
	this->stak = new Node*[this->capacity];
}

// Mazani stromu
void BSTree::Node::deleteTree(Node*& root) {
	if (root == NULL)
		return;

	deleteTree(root->left);
	deleteTree(root->right);

	cout << "del key: " << root->key << endl;

	delete[] root;
	root = nullptr;
}

BSTree::~BSTree()
{
	for (int i = 0; i < this->top; i++) {
		stak[i] = nullptr;
	}

	delete stak;
	stak = nullptr;
	this->root->deleteTree(this->root);
}
