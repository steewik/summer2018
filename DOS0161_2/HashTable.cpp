#include "HashTable.h"
#include <vector>
#include <functional>
using namespace std;

const int DEFAULT_MAX_CAPACITY = 11;

HashTable::HashTable() {
	this->size = DEFAULT_MAX_CAPACITY;
	this->table.resize(this->size);
}

HashTable::HashTable(int size) {
	this->size = size;
	this->table.resize(this->size);
}

HashTable::~HashTable() {
	cout << "Tabulka byla odstranena" << endl;
}

void HashTable::insert(string s) {
	hash<string> hash_fn;
	size_t hash = hash_fn(s);
	this->table[hash%this->size].push_back(s);
}

bool HashTable::search(string s) {
	hash<string> hash_fn;
	size_t hash = hash_fn(s);
	if (find(this->table[hash%this->size].begin(), this->table[hash%this->size].end(), s) != this->table[hash%this->size].end())
		return true;
	else
		return false;
}

int HashTable::getMaxSize() {
	return this->size;
}

void HashTable::report() {
	for (int i = 0; i < this->size; i++) {
		cout << "[" << i << "] -> ";
		for (int j = 0; j < this->table[i].size() ; j++) {
			if (j) cout << " -> ";
			cout << this->table[i][j];
		}
		cout << endl;
	}
}